
insert into `usuarios` (nombre, password) VALUES ('admin','$2a$10$DOMDxjYyfZ/e7RcBfUpzqeaCs8pLgcizuiQWXPkU35nOhZlFcE9MS');
insert into `usuarios` (nombre, password) VALUES ('isra','$2a$10$DOMDxjYyfZ/e7RcBfUpzqeaCs8pLgcizuiQWXPkU35nOhZlFcE9MS');

insert into `paradas` (num_parada, calle, latitud, longitud) values (1, 'Calle Alcalá 5', '40.379054', '-3.612771');
insert into `paradas` (num_parada, calle, latitud, longitud) values (2, 'Calle Alcalá 20', '40.378943', '-3.612862');
insert into `paradas` (num_parada, calle, latitud, longitud) values (3, 'Calle Alcalá 55', '40.378831', '-3.612953');
insert into `paradas` (num_parada, calle, latitud, longitud) values (4, 'Calle Alcalá 76', '40.378811', '-3.613033');
insert into `paradas` (num_parada, calle, latitud, longitud) values (5, 'Calle Alcalá 100', '40.378777', '-3.613124');
insert into `paradas` (num_parada, calle, latitud, longitud) values (6, 'Calle Generál Aguilera 60', '40.373958', '-3.612761');
insert into `paradas` (num_parada, calle, latitud, longitud) values (7, 'Calle Generál Aguilera 101', '40.373856', '-3.612552');
insert into `paradas` (num_parada, calle, latitud, longitud) values (8, 'Calle Generál Aguilera 105', '40.373755', '-3.612343');
insert into `paradas` (num_parada, calle, latitud, longitud) values (9, 'Calle Generál Aguilera 109', '40.373654', '-3.612235');
insert into `paradas` (num_parada, calle, latitud, longitud) values (10, 'Calle Generál Aguilera 142', '40.373053', '-3.611727');


insert into `lineas` (identificador, nombre_linea, num_parada, orden) values ('143', '143', 1, 1);
insert into `lineas` (identificador, nombre_linea, num_parada, orden) values ('143', '143', 6, 2);
insert into `lineas` (identificador, nombre_linea, num_parada, orden) values ('143', '143', 4, 3);
insert into `lineas` (identificador, nombre_linea, num_parada, orden) values ('143', '143', 10, 4);
insert into `lineas` (identificador, nombre_linea, num_parada, orden) values ('143', '143', 9, 5);
insert into `lineas` (identificador, nombre_linea, num_parada, orden) values ('180', '180', 1, 1);
insert into `lineas` (identificador, nombre_linea, num_parada, orden) values ('180', '180', 6, 2);
insert into `lineas` (identificador, nombre_linea, num_parada, orden) values ('180', '180', 4, 3);
insert into `lineas` (identificador, nombre_linea, num_parada, orden) values ('180', '180', 10, 4);
insert into `lineas` (identificador, nombre_linea, num_parada, orden) values ('180', '180', 9, 5);

insert into `lineas` (identificador, nombre_linea, num_parada, orden) values ('180', '180', 7, 6);
insert into `lineas` (identificador, nombre_linea, num_parada, orden) values ('180', '180', 3, 7);
insert into `lineas` (identificador, nombre_linea, num_parada, orden) values ('180', '180', 2, 8);
insert into `lineas` (identificador, nombre_linea, num_parada, orden) values ('180', '180', 8, 9);
insert into `lineas` (identificador, nombre_linea, num_parada, orden) values ('180', '180', 5, 10);


insert into `autobuses` (matricula, latitud, longitud, linea_asignada_identificador, linea_asignada_orden) values ('2799KRB', '40.379605', '-3.620907', '143', 1);
insert into `autobuses` (matricula, latitud, longitud, linea_asignada_identificador, linea_asignada_orden) values ('1234ABC', '40.379474', '-3.618332', '143', 2);
insert into `autobuses` (matricula, latitud, longitud, linea_asignada_identificador, linea_asignada_orden) values ('4325DEF', '40.379340', '-3.615455', '143', 3);
insert into `autobuses` (matricula, latitud, longitud, linea_asignada_identificador, linea_asignada_orden) values ('8733JTD', '40.379054', '-3.612730', '143', 4);
insert into `autobuses` (matricula, latitud, longitud, linea_asignada_identificador, linea_asignada_orden) values ('2709KRB', '40.379605', '-3.620907', '180', 8);
insert into `autobuses` (matricula, latitud, longitud, linea_asignada_identificador, linea_asignada_orden) values ('1214ABC', '40.379474', '-3.618332', '180', 7);
insert into `autobuses` (matricula, latitud, longitud, linea_asignada_identificador, linea_asignada_orden) values ('4335DEF', '40.379340', '-3.615455', '180', 6);
insert into `autobuses` (matricula, latitud, longitud, linea_asignada_identificador, linea_asignada_orden) values ('8743JTD', '40.379054', '-3.612730', '180', 9);
insert into `autobuses` (matricula, latitud, longitud, linea_asignada_identificador, linea_asignada_orden) values ('2759KRB', '40.379605', '-3.620907', '180', 1);
insert into `autobuses` (matricula, latitud, longitud, linea_asignada_identificador, linea_asignada_orden) values ('1264ABC', '40.379474', '-3.618332', '180', 2);
insert into `autobuses` (matricula, latitud, longitud, linea_asignada_identificador, linea_asignada_orden) values ('4375DEF', '40.379340', '-3.615455', '180', 3);
insert into `autobuses` (matricula, latitud, longitud, linea_asignada_identificador, linea_asignada_orden) values ('8783JTD', '40.379054', '-3.612730', '180', 4);




