package edu.viu.autobusesposicion.front.lineas.service;

import java.util.List;

import edu.viu.autobusesposicion.front.lineas.bean.LineaBean;
import edu.viu.autobusesposicion.front.lineas.bean.LineaId;

public interface ILineaService {

    String URL_BASE = "http://localhost:8080";
	String URL_LINEAS = URL_BASE + "/lineas/lista-lineas?numParada=";
	String URL_LINEA = URL_BASE + "/lineas/linea";

    List<LineaBean> buscarLineasPorParada(int numParada);

    LineaBean buscarLinea(LineaId lineaId);
}
