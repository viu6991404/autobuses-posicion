package edu.viu.autobusesposicion.front.paradas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import edu.viu.autobusesposicion.front.paradas.bean.ParadaBean;
import edu.viu.autobusesposicion.front.paradas.service.IParadaService;

/**
 * Controlador para ventanas sobre paradas
 */
@Controller
@RequestMapping("/paradas")
@SessionAttributes("paradas")
public class ParadaController {

	@Autowired
	private IParadaService paradaService;

	/**
	 * Devuelve la pantalla de listado de paradas
	 *
	 * @param model model de spring
	 * @return Pantalla a cargar
	 */
	@GetMapping(value = "/lista-paradas")
	public String listarParadas(Model model) {
		List<ParadaBean> paradas = paradaService.buscarTodasParadas();

		model.addAttribute("titulo", "Listado de paradas");
		model.addAttribute("paradas", paradas);
		model.addAttribute("parada", new ParadaBean());
		return "pantallas/paradas.html";
	}
}
