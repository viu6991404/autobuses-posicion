package edu.viu.autobusesposicion.front.paradas.service;

import java.util.List;

import edu.viu.autobusesposicion.front.paradas.bean.ParadaBean;

public interface IParadaService {

    String URL_BASE = "http://localhost:8080";
    String URL_PARADAS = URL_BASE + "/paradas/lista-paradas";
    String URL_PARADA = URL_BASE + "/paradas/parada";

    List<ParadaBean> buscarTodasParadas();

    ParadaBean buscarParada(int numParada);
}
