package edu.viu.autobusesposicion.front.usuarios.service;

import java.util.List;

import edu.viu.autobusesposicion.front.usuarios.bean.UsuarioBean;

public interface IUsuarioService {

    List<UsuarioBean> buscarTodosUsuarios();

    UsuarioBean buscarPorNombre(String nombre);
}
