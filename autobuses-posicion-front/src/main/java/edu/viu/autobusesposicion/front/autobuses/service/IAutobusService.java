package edu.viu.autobusesposicion.front.autobuses.service;

import edu.viu.autobusesposicion.front.autobuses.bean.AutobusBean;
import edu.viu.autobusesposicion.front.lineas.bean.LineaId;

public interface IAutobusService {

    String URL_BASE = "http://localhost:8080";
    String URL_AUTOBUS_LINEA = URL_BASE + "/autobus/linea";

    AutobusBean buscarAutobusPorLinea(LineaId linea);
}
