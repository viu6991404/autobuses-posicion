package edu.viu.autobusesposicion.front.lineas.bean;

import java.io.Serializable;

/**
 * Objeto representación del ID de líneas de autobús
 */
public class LineaId implements Serializable {

    private static final long serialVersionUID = 1L;

    private String identificador;

    private int orden;

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public LineaId(String identificador, int orden) {
        this.identificador = identificador;
        this.orden = orden;
    }

    public LineaId() {
    }

}
