package edu.viu.autobusesposicion.front.usuarios.bean;

import java.io.Serializable;

/**
 * Objeto representación del usuario
 */
public class UsuarioBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nombre;

	private String password;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
