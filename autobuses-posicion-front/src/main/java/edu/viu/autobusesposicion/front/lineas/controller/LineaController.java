package edu.viu.autobusesposicion.front.lineas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import edu.viu.autobusesposicion.front.lineas.bean.LineaBean;
import edu.viu.autobusesposicion.front.lineas.service.ILineaService;
import edu.viu.autobusesposicion.front.paradas.service.IParadaService;

/**
 * Controlador para ventanas sobre linea
 */
@Controller
@RequestMapping("/lineas")
@SessionAttributes("linea")
public class LineaController {

	@Autowired
	private ILineaService lineaService;

	@Autowired
	private IParadaService paradaService;

	/**
	 * Devuelve la pantalla de listado de lineas con datos cargados
	 *
	 * @param model model de spring
	 * @return Pantalla a cargar
	 */
	@PostMapping(value = "/lista-lineas")
	public String listarLineas(Model model, @RequestParam(name = "parada") int numParada) {
		List<LineaBean> lineas = lineaService.buscarLineasPorParada(numParada);

		model.addAttribute("titulo", "Listado de lineas");
		model.addAttribute("lineas", lineas);
		model.addAttribute("parada", paradaService.buscarParada(numParada));
		return "pantallas/lineas.html";
	}
}
