package edu.viu.autobusesposicion.front.usuarios.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import edu.viu.autobusesposicion.front.usuarios.bean.UsuarioBean;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

	private RestTemplate restTemplate = new RestTemplate();

	String URL_BASE = "http://localhost:8080";
	String URL_Usuarios = URL_BASE + "/lineas/lista-lineas";

	/**
	 * Busca todos los usuarios
	 *
	 * @return Devuelve el listado de usuarios
	 */
	@Override
	public List<UsuarioBean> buscarTodosUsuarios() {
		return Arrays.asList(restTemplate.postForEntity(URL_Usuarios, null, UsuarioBean[].class).getBody());
	}

	/**
	 * Busca una usuario por nombre
	 *
	 * @param nombre Nombre identificador del usuario
	 * @return Devuelve el usuario
	 */
	@Override
	public UsuarioBean buscarPorNombre(String nombre) {
		return restTemplate.postForEntity(URL_Usuarios, nombre, UsuarioBean.class).getBody();
	}
}
