package edu.viu.autobusesposicion.front.lineas.bean;

import java.io.Serializable;

public class LineaBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private LineaId id;

    private String nombreLinea;

    private int numParada;

    public String getNombreLinea() {
        return nombreLinea;
    }

    public void setNombreLinea(String nombreLinea) {
        this.nombreLinea = nombreLinea;
    }

    public LineaId getId() {
        return id;
    }

    public void setId(LineaId id) {
        this.id = id;
    }

    public int getNumParada() {
        return numParada;
    }

    public void setNumParada(int numParada) {
        this.numParada = numParada;
    }
}
