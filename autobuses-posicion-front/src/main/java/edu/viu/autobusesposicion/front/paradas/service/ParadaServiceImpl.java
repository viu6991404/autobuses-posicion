package edu.viu.autobusesposicion.front.paradas.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import edu.viu.autobusesposicion.front.paradas.bean.ParadaBean;

@Service
public class ParadaServiceImpl implements IParadaService {

	private RestTemplate restTemplate = new RestTemplate();

	/**
	 * Busca todas las paradas
	 *
	 * @return Devuelve el listado de paradas
	 */
	@Override
	public List<ParadaBean> buscarTodasParadas() {
		return Arrays.asList(restTemplate.getForEntity(URL_PARADAS, ParadaBean[].class).getBody());
	}

	public ParadaBean buscarParada(int numParada) {
		return restTemplate.getForEntity(URL_PARADA + "?numParada=" + numParada, ParadaBean.class).getBody();
	}
}
