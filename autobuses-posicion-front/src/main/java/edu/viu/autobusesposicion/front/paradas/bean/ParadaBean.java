package edu.viu.autobusesposicion.front.paradas.bean;

import java.io.Serializable;
import java.util.List;

import edu.viu.autobusesposicion.front.lineas.bean.LineaBean;

/**
 * Objeto representación de las paradas
 */
public class ParadaBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private int numParada;

    private String calle;

    private String latitud;

    private String longitud;

    private List<LineaBean> lineas;

    public int getNumParada() {
        return numParada;
    }

    public void setNumParada(int numParada) {
        this.numParada = numParada;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public List<LineaBean> getLineas() {
        return lineas;
    }

    public void setLineas(List<LineaBean> lineas) {
        this.lineas = lineas;
    }

}
