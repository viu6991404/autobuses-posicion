package edu.viu.autobusesposicion.front.autobuses.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import edu.viu.autobusesposicion.front.autobuses.bean.AutobusBean;
import edu.viu.autobusesposicion.front.lineas.bean.LineaId;

@Service
public class AutobusServiceImpl implements IAutobusService {

	private RestTemplate restTemplate = new RestTemplate();

	/**
	 * Busca un autobús por su línea
	 *
	 * @return Devuelve el autobús
	 */
	@Override
	public AutobusBean buscarAutobusPorLinea(LineaId linea) {
		return restTemplate.postForEntity(URL_AUTOBUS_LINEA, linea, AutobusBean.class).getBody();
	}
}
