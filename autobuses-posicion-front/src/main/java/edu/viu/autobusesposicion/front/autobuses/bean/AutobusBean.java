package edu.viu.autobusesposicion.front.autobuses.bean;

import java.io.Serializable;

import edu.viu.autobusesposicion.front.lineas.bean.LineaBean;

/**
 * Bean representación de los autobuses
 */
public class AutobusBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String matricula;

    private String latitud;

    private String longitud;

    private LineaBean lineaAsignada;

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public LineaBean getLineaAsignada() {
        return lineaAsignada;
    }

    public void setLineaAsignada(LineaBean lineaAsignada) {
        this.lineaAsignada = lineaAsignada;
    }

}
