package edu.viu.autobusesposicion.front.autobuses.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import edu.viu.autobusesposicion.front.autobuses.service.IAutobusService;
import edu.viu.autobusesposicion.front.lineas.bean.LineaId;
import edu.viu.autobusesposicion.front.lineas.service.ILineaService;

/**
 * Controlador para ventanas sobre aubotuses
 */
@Controller
@RequestMapping("/autobus")
@SessionAttributes("autobus")
public class AutobusController {

	@Autowired
	private IAutobusService autobusService;

	@Autowired
	private ILineaService lineaService;

	/**
	 * Devuelve la pantalla de autobuse por linea y parada
	 *
	 * @param model model de spring
	 * @return Pantalla a cargar
	 */
	@PostMapping(value = "/linea")
	public String buscarAutobusPorLinea(Model model, @RequestParam(value = "linea") String lineaIdConcat) {
		LineaId lineaId = separaLineaId(lineaIdConcat);

		model.addAttribute("titulo", "Información del autobus");
		model.addAttribute("autobus", autobusService.buscarAutobusPorLinea(lineaId));
		model.addAttribute("linea", lineaService.buscarLinea(lineaId));
		return "pantallas/autobus.html";
	}

	private LineaId separaLineaId(String lineaIdConcat) {
		String[] arrLineaId = lineaIdConcat.split("#");
		return new LineaId(arrLineaId[0], Integer.valueOf(arrLineaId[1]));
	}
}
