package edu.viu.autobusesposicion.front.login.service;

import java.util.ArrayList;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import edu.viu.autobusesposicion.front.usuarios.bean.UsuarioBean;

/**
 * Servicio para el control del login
 */
@Service
public class LoginService implements UserDetailsService {

	private RestTemplate restTemplate = new RestTemplate();

	private final static String URL_BASE = "http://localhost:8080";
	private final static String URL_usuarios = URL_BASE + "/usuario";

	/**
	 * Carga los datos del usuario por nombre
	 *
	 * @param nombre Identificador del usuario
	 * @return Devueve el usuario
	 * @throws UsernameNotFoundException Exepción al no encontrar al usuario
	 */
	@Override
	public UserDetails loadUserByUsername(String nombre) throws UsernameNotFoundException {
		UsuarioBean usuario = restTemplate.postForEntity(URL_usuarios, nombre, UsuarioBean.class).getBody();
		if (null != usuario) {
			return new User(usuario.getNombre(), usuario.getPassword(), true, true, true, true,
					new ArrayList<>());
		} else {
			throw new UsernameNotFoundException("Nombre de usuario no encontrado");
		}
	}

}
