package edu.viu.autobusesposicion.front.lineas.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import edu.viu.autobusesposicion.front.lineas.bean.LineaBean;
import edu.viu.autobusesposicion.front.lineas.bean.LineaId;

@Service
public class LineaServiceImpl implements ILineaService {

	private RestTemplate restTemplate = new RestTemplate();

	@Override
	public List<LineaBean> buscarLineasPorParada(int numParada) {
		return Arrays.asList(restTemplate.getForEntity(URL_LINEAS + numParada, LineaBean[].class).getBody());
	}

	@Override
	public LineaBean buscarLinea(LineaId lineaId) {
		return restTemplate.postForEntity(URL_LINEA, lineaId, LineaBean.class).getBody();
	}
}