package edu.viu.autobusesposicion.back.paradas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.viu.autobusesposicion.back.paradas.entity.Parada;
import edu.viu.autobusesposicion.back.paradas.service.IParadaService;

/**
 * Controlador para ventanas sobre paradas
 */
@Controller
@RequestMapping("/paradas")
public class ParadaRestController {

	@Autowired
	private IParadaService paradaService;

	/**
	 * Devuelve el listado de paradas
	 *
	 * @param model model de spring
	 * @return listado de paradas
	 */
	@GetMapping(value = "/lista-paradas")
	@ResponseBody
	public List<Parada> listarParadas() {
		return paradaService.buscarTodasParadas();
	}

	/**
	 * Devuelve una paradas
	 *
	 * @param numParada número de la parada a buscar
	 * @return Parada
	 */
	@GetMapping(value = "/parada")
	@ResponseBody
	public Parada buscarParada(@RequestParam Integer numParada) {
		return paradaService.buscarParada(numParada);
	}
}
