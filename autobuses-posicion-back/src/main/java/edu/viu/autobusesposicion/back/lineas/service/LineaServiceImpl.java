package edu.viu.autobusesposicion.back.lineas.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.viu.autobusesposicion.back.lineas.entity.Linea;
import edu.viu.autobusesposicion.back.lineas.entity.LineaId;
import edu.viu.autobusesposicion.back.lineas.repository.ILineaRepository;

@Service
public class LineaServiceImpl implements ILineaService {

	@Autowired
	private ILineaRepository lineaRepository;

	@Override
	@Transactional(readOnly = true)
	public List<Linea> buscarTodasLineas() {
		return (List<Linea>) lineaRepository.findAll();
	}

	@Override
	public List<Linea> buscarLineasPorParada(int numParada) {
		List<Linea> listaLineas = (List<Linea>) lineaRepository.findAll();
		return listaLineas.stream().filter(l -> l.getNumParada() == numParada).collect(Collectors.toList());
	}

	@Override
	public Linea buscarLinea(LineaId lineaId) {
		return lineaRepository.findById(lineaId).orElse(null);
	}

}
