package edu.viu.autobusesposicion.back.autobuses.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.viu.autobusesposicion.back.autobuses.entity.Autobus;
import edu.viu.autobusesposicion.back.autobuses.repository.IAutobusRepository;
import edu.viu.autobusesposicion.back.lineas.entity.LineaId;

@Service
public class AutobusServiceImpl implements IAutobusService {

	@Autowired
	private IAutobusRepository autobusRepository;

	/**
	 * Busca un autobús por su línea
	 *
	 * @return Devuelve el autobús
	 */
	@Override
	@Transactional(readOnly = true)
	public Autobus buscarAutobusPorLinea(LineaId linea) {

		return autobusRepository.buscarAutobusPorLinea(linea.getIdentificador(), linea.getOrden());
		// return autobusRepository.findById(linea.getIdentificador(),
		// linea.getOrden()).orElse(null);
	}

	/**
	 * Actualiza la ubicación del autobús
	 *
	 * @return Devuelve el autobús
	 */
	@Override
	@Transactional(readOnly = false)
	public void actualizaPosicionAutobus(String matricula, String latitud, String longitud) {
		Autobus autobus = new Autobus();
		autobus.setMatricula(matricula);
		autobus.setLatitud(latitud);
		autobus.setLongitud(longitud);
		autobusRepository.save(autobus);
	}
}
