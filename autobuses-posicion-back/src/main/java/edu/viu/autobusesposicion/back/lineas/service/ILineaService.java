package edu.viu.autobusesposicion.back.lineas.service;

import java.util.List;

import edu.viu.autobusesposicion.back.lineas.entity.Linea;
import edu.viu.autobusesposicion.back.lineas.entity.LineaId;

public interface ILineaService {

    List<Linea> buscarTodasLineas();

    List<Linea> buscarLineasPorParada(int numParada);

    Linea buscarLinea(LineaId lineaId);
}
