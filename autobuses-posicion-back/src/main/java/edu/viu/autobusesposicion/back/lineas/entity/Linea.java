package edu.viu.autobusesposicion.back.lineas.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Objeto representación de las líneas de autobús
 */
@Entity
@Table(name = "lineas")
@Embeddable
public class Linea implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private LineaId id;

    @Column
    private String nombreLinea;

    @Column
    private int numParada;

    public String getNombreLinea() {
        return nombreLinea;
    }

    public void setNombreLinea(String nombreLinea) {
        this.nombreLinea = nombreLinea;
    }

    public LineaId getId() {
        return id;
    }

    public void setId(LineaId id) {
        this.id = id;
    }

    public int getNumParada() {
        return numParada;
    }

    public void setNumParada(int numParada) {
        this.numParada = numParada;
    }

}