package edu.viu.autobusesposicion.back.lineas.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

/**
 * Objeto representación de la clave de líneas de autobús
 */
@Embeddable
public class LineaId implements Serializable {

    private static final long serialVersionUID = 1L;

    private String identificador;

    private int orden;

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public LineaId(String identificador, int orden) {
        this.identificador = identificador;
        this.orden = orden;
    }

    public LineaId() {
    }

}