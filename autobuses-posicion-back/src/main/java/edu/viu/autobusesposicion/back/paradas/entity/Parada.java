package edu.viu.autobusesposicion.back.paradas.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import edu.viu.autobusesposicion.back.lineas.entity.Linea;

/**
 * Objeto representación de las paradas
 */
@Entity
@Table(name = "Paradas")
public class Parada implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(length = 7, unique = true, nullable = false, updatable = false)
    private int numParada;

    @Column
    private String calle;

    @Column
    private String latitud;

    @Column
    private String longitud;

    @OneToMany
    private List<Linea> lineas;

    public int getNumParada() {
        return numParada;
    }

    public void setNumParada(int numParada) {
        this.numParada = numParada;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public List<Linea> getLineas() {
        return lineas;
    }

    public void setLineas(List<Linea> lineas) {
        this.lineas = lineas;
    }

}
