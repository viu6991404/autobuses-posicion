package edu.viu.autobusesposicion.back.autobuses.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import edu.viu.autobusesposicion.back.lineas.entity.Linea;

/**
 * Objeto representación de los autobuses
 */
@Entity
@Table(name = "autobuses")
public class Autobus implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(length = 7, unique = true, nullable = false, updatable = false)
    private String matricula;

    @Column
    private String latitud;

    @Column
    private String longitud;

    @OneToOne
    private Linea lineaAsignada;

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public Linea getLineaAsignada() {
        return lineaAsignada;
    }

    public void setLineaAsignada(Linea lineaAsignada) {
        this.lineaAsignada = lineaAsignada;
    }

}
