package edu.viu.autobusesposicion.back.paradas.repository;

import org.springframework.data.repository.CrudRepository;

import edu.viu.autobusesposicion.back.paradas.entity.Parada;

/**
 * Repositorio de parada para acceder a bbdd
 */
public interface IParadaRepository extends CrudRepository<Parada, Integer> {
}
