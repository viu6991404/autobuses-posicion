package edu.viu.autobusesposicion.back.autobuses.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import edu.viu.autobusesposicion.back.autobuses.entity.Autobus;

/**
 * Repositorio de autobus para acceder a bbdd
 */
public interface IAutobusRepository extends CrudRepository<Autobus, String> {

    @Query("select b from Autobus b where linea_asignada_identificador = :identificador and linea_asignada_orden = :orden")
    Autobus buscarAutobusPorLinea(String identificador, int orden);
}
