package edu.viu.autobusesposicion.back.usuarios.repository;

import org.springframework.data.repository.CrudRepository;

import edu.viu.autobusesposicion.back.usuarios.entity.Usuario;

/**
 * Repositorio de usuarios para acceder a bbdd
 */
public interface IUsuarioRepository extends CrudRepository<Usuario, String> {
}
