package edu.viu.autobusesposicion.back.usuarios.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.viu.autobusesposicion.back.usuarios.entity.Usuario;
import edu.viu.autobusesposicion.back.usuarios.repository.IUsuarioRepository;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

	@Autowired
	private IUsuarioRepository usuarioRepository;

	/**
	 * Busca todos los usuarios
	 *
	 * @return Devuelve el listado de usuarios
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Usuario> buscarTodosUsuarios() {
		return (List<Usuario>) usuarioRepository.findAll();
	}

	/**
	 * Busca una usuario por nombre
	 *
	 * @param nombre Nombre identificador del usuario
	 * @return Devuelve el usuario
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<Usuario> buscarPorNombre(String nombre) {
		return usuarioRepository.findById(nombre);
	}
}
