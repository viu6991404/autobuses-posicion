package edu.viu.autobusesposicion.back.lineas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.viu.autobusesposicion.back.lineas.entity.Linea;
import edu.viu.autobusesposicion.back.lineas.entity.LineaId;
import edu.viu.autobusesposicion.back.lineas.service.ILineaService;

/**
 * Controlador de lineas
 */
@Controller
@RequestMapping("/lineas")
public class LineaRestController {

	@Autowired
	private ILineaService lineaService;

	/**
	 * Devuelve un listado de lineas con datos cargados
	 *
	 * @param numParada número de la parada a consultar sus líneas
	 * @return Listado de lineas
	 */
	@GetMapping("/lista-lineas")
	@ResponseBody
	public List<Linea> listarLineas(@RequestParam Integer numParada) {
		return lineaService.buscarLineasPorParada(numParada);
	}

	/**
	 * Devuelve una linea
	 * 
	 * @param lineaId
	 * @return Línea consultada
	 */
	@PostMapping("/linea")
	@ResponseBody
	public Linea listarLineas(@RequestBody LineaId lineaId) {
		return lineaService.buscarLinea(lineaId);
	}
}
