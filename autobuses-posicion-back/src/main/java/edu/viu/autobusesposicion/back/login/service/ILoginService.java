package edu.viu.autobusesposicion.back.login.service;

import java.util.Optional;

import edu.viu.autobusesposicion.back.usuarios.entity.Usuario;

public interface ILoginService {
    Optional<Usuario> loadUserByUsername(String nombre);
}
