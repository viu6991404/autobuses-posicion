package edu.viu.autobusesposicion.back.autobuses.service;

import edu.viu.autobusesposicion.back.autobuses.entity.Autobus;
import edu.viu.autobusesposicion.back.lineas.entity.LineaId;

public interface IAutobusService {

    Autobus buscarAutobusPorLinea(LineaId linea);

    void actualizaPosicionAutobus(String matricula, String latitud, String longitud);
}
