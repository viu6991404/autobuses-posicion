package edu.viu.autobusesposicion.back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Clase para lanzar la aplicación
 */
@SpringBootApplication
public class BackRun {

	/**
	 * Lanzador de la aplicación
	 *
	 * @param args Argumentos de la línea de comandos
	 */
	public static void main(String[] args) {
		SpringApplication.run(BackRun.class, args);
	}
}
