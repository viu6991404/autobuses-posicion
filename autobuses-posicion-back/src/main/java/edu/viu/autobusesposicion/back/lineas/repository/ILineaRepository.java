package edu.viu.autobusesposicion.back.lineas.repository;

import org.springframework.data.repository.CrudRepository;

import edu.viu.autobusesposicion.back.lineas.entity.Linea;
import edu.viu.autobusesposicion.back.lineas.entity.LineaId;

/**
 * Repositorio de líneas para acceder a bbdd
 */
public interface ILineaRepository extends CrudRepository<Linea, LineaId> {
}
