package edu.viu.autobusesposicion.back.usuarios.service;

import java.util.List;
import java.util.Optional;

import edu.viu.autobusesposicion.back.usuarios.entity.Usuario;

public interface IUsuarioService {

    List<Usuario> buscarTodosUsuarios();

    Optional<Usuario> buscarPorNombre(String nombre);
}
