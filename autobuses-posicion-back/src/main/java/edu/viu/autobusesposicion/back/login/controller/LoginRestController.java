package edu.viu.autobusesposicion.back.login.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.viu.autobusesposicion.back.login.service.LoginServiceImpl;
import edu.viu.autobusesposicion.back.usuarios.entity.Usuario;

/**
 * Controlador para el login
 */
@Controller("/usuario")
public class LoginRestController {

    @Autowired
    private LoginServiceImpl loginService;

    /**
     * Se encarga de recibir las peticiones de login de usuario
     * 
     * @return Devuelve los detalles del usuario
     */
    @PostMapping()
    @ResponseBody
    public ResponseEntity<Optional<Usuario>> login(@RequestBody() String nombre) {
        return ResponseEntity.ok(loginService.loadUserByUsername(nombre));
    }

}
