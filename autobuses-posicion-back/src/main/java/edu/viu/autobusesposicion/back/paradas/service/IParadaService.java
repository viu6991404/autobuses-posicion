package edu.viu.autobusesposicion.back.paradas.service;

import java.util.List;

import edu.viu.autobusesposicion.back.paradas.entity.Parada;

public interface IParadaService {

    List<Parada> buscarTodasParadas();

    Parada buscarParada(int numParada);
}
