package edu.viu.autobusesposicion.back.login.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.viu.autobusesposicion.back.usuarios.entity.Usuario;
import edu.viu.autobusesposicion.back.usuarios.service.IUsuarioService;

/**
 * Servicio para el control del login
 */
@Service
public class LoginServiceImpl implements ILoginService {

	@Autowired
	private IUsuarioService usuarioServiceImpl;

	/**
	 * Carga los datos del usuario por nombre
	 *
	 * @param nombre Identificador del usuario
	 * @return Devueve el usuario
	 */
	@Override
	@Transactional(readOnly = false)
	public Optional<Usuario> loadUserByUsername(String nombre) {
		return usuarioServiceImpl.buscarPorNombre(nombre);
	}

}
