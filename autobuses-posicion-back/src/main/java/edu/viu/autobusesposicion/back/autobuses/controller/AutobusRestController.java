package edu.viu.autobusesposicion.back.autobuses.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.viu.autobusesposicion.back.autobuses.entity.Autobus;
import edu.viu.autobusesposicion.back.autobuses.service.IAutobusService;
import edu.viu.autobusesposicion.back.lineas.entity.LineaId;

/**
 * RestController sobre aubotuses
 */
@Controller
@RequestMapping("/autobus")
public class AutobusRestController {

	@Autowired
	private IAutobusService autobusService;

	/**
	 * Devuelve la pantalla de autobuse por linea y parada
	 *
	 * @param LineaId Id de la línea a consultar
	 * @return Bean del autobús
	 */
	@PostMapping("/linea")
	@ResponseBody
	public Autobus buscarAutobusPorLinea(@RequestBody LineaId lineaId) {
		return autobusService.buscarAutobusPorLinea(lineaId);
	}

	/**
	 * Guarda la posición del autobus
	 *
	 * @param matricula Matricula del autobus a guardar
	 * @param latitud   Latitud del autobus a actualizar
	 * @param longitud  Latitud del autobus a actualizar
	 */
	@PostMapping("/editar-posicion-autobus")
	@ResponseBody
	public void guardar(String matricula, String latitud, String longitud) {
		autobusService.actualizaPosicionAutobus(matricula, latitud, longitud);
	}
}
