package edu.viu.autobusesposicion.back.paradas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.viu.autobusesposicion.back.paradas.entity.Parada;
import edu.viu.autobusesposicion.back.paradas.repository.IParadaRepository;

@Service
public class ParadaServiceImpl implements IParadaService {

	@Autowired
	private IParadaRepository paradaRepository;

	/**
	 * Busca todas las paradas
	 *
	 * @return Devuelve el listado de paradas
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Parada> buscarTodasParadas() {
		return (List<Parada>) paradaRepository.findAll();
	}

	/**
	 * busca una paradas
	 * 
	 * @param numParada número de la parada a consultar
	 * @return Devuelve una paradas
	 */
	@Override
	public Parada buscarParada(int numParada) {
		return paradaRepository.findById(numParada).orElse(null);
	}
}
